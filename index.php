<!DOCTYPE html>
<html>
<head>
	<?php include("includes/head.php")?>
</head>
<body>
    <?php include("includes/load.php")?>
    <?php include("includes/menu.php")?>

	<!-- HERO SECTION --> 
	<section class="hero col-md-12">
        <div class="bg-video" data-property="{videoURL:'https://youtu.be/IsyJ3DwY3_g',
            showControls: false,
            containment:'self',
            startAt:1,
            mute:true,
            autoPlay:true,
            loop:true,
            opacity:1,
            optimizeDisplay:true,
            quality:'hd1080'}"></div>
        <div class="bg-image bg-video-placeholder"><img src="img/home.jpg" alt=""></div>
		<div class="hero-inner">
			<h1> Criamos <br>projetos <span class="bg-dynamic-text">incríveis</span> </h1>
            <p>Think of a <span>creative digital agency</span> in Madrid, <br> turning your dreams into reality.</p>
            <a href="sobre.php" id="con_submit" class="sitebtn top_30 btn-about" type="submit">Conheça-nos</a>
            <div id="line-scroll" style="display: block;">
                <div class="line-scroll" id="lsl"> <span class="line active"></span> </div>
            </div>
		</div>
	</section>

	<div class="wrapper container">

	<!-- PORTFOLIO SECTION -->	
	<section class="portfolio col-md-12">
		<!-- Portfolio Filter -->
		<div class="col-md-3">
            <div class="left-filter-section">
    			<div class="page-title left">
                    <span>PORTFÓLIO</span>
                    <h2 class="title">Nós criamos <br> experiências</h2>
                </div>
            	<div class="portfolio-filter">
                    <ul>
                        <li class="select-cat" data-filter="*">Todos</li>
                        <li data-filter=".websites">Websites</li>
                        <li data-filter=".conceituais">Conceituais</li>
                        <!-- <li data-filter=".clinicas">Clínicas</li> -->
                        <li data-filter=".dashboards">Dashboards</li>
                        <li data-filter=".aplicativos">Aplicativos</li>
                        <li data-filter=".ecommerces">E-commerce</li>
                    </ul>
                </div>
            </div>
		</div>
		<!-- Images -->
		<div class="col-md-9">
            <div class="isotope_items row">
                <!-- Item -->
                <a href="projeto.php" class="col-md-6 col-sm-6 col-xs-12 single_item conceituais">
                    <figure>
                        <img src="img/work-1.jpg" alt="">
                        <figcaption>
                            <h3>Save the nature</h3>
                            <p>Conceitual</p>
                        </figcaption>
                    </figure>
                </a>
                <!-- Item -->
                <a href="projeto.php" class="col-md-6 col-sm-6 col-xs-12 single_item websites">
                    <figure>
                        <img src="img/work-2.jpg" alt="">
                        <figcaption>
                            <h3>Interativa FM</h3>
                            <p>Websites </p>
                        </figcaption>
                    </figure>
                </a>
                <!-- Item -->
                <a href="projeto.php" class="col-md-6 col-sm-6 col-xs-12 single_item aplicativos">
                    <figure>
                        <img src="img/work-3.jpg" alt="">
                        <figcaption>
                            <h3>Paint Art</h3>
                            <p>Aplicativos</p>
                        </figcaption>
                    </figure>
                </a>
                <!-- Item -->
                <a href="projeto.php" class="col-md-6 col-sm-6 col-xs-12 single_item websites">
                    <figure>
                        <img src="img/work-4.jpg" alt="">
                        <figcaption>
                            <h3>Escola Militar - IOPEM</h3>
                            <p>Websites </p>
                        </figcaption>
                    </figure>
                </a>
                <!-- Item -->
                <a href="projeto.php" class="col-md-6 col-sm-6 col-xs-12 single_item websites">
                    <figure>
                        <img src="img/work-5.jpg" alt="">
                        <figcaption>
                            <h3>Reactioon</h3>
                            <p>Websites </p>
                        </figcaption>
                    </figure>
                </a>
                <!-- Item -->
                <a href="projeto.php" class="col-md-6 col-sm-6 col-xs-12 single_item ecommerces">
                    <figure>
                        <img src="img/work-6.jpg" alt="">
                        <figcaption>
                            <h3>Compex Brasil</h3>
                            <p>E-commerces </p>
                        </figcaption>
                    </figure>
                </a>
                <!-- Item -->
                <a href="projeto.php" class="col-md-6 col-sm-6 col-xs-12 single_item conceituais">
                    <figure>
                        <img src="img/work-7.jpg" alt="">
                        <figcaption>
                            <h3>The Space</h3>
                            <p>Conceitual </p>
                        </figcaption>
                    </figure>
                </a>
                <!-- Item -->
                <a href="work-9.html" class="col-md-6 col-sm-6 col-xs-12 single_item websites">
                    <figure>
                        <img src="img/work-8.jpg" alt="">
                        <figcaption>
                            <h3>Agência Back</h3>
                            <p>Websites </p>
                        </figcaption>
                    </figure>
                </a>
                <!-- Item -->
                <a href="projeto.php" class="col-md-6 col-sm-6 col-xs-12 single_item dashboards">
                    <figure>
                        <img src="img/work-9.jpg" alt="">
                        <figcaption>
                            <h3>Mobisis</h3>
                            <p>Dashboards </p>
                        </figcaption>
                    </figure>
                </a>
            </div>
        </div>
	</section>
	</div>

	<?php include("includes/footer.php")?>
	<?php include("includes/scripts.php")?>
    <script src="vendor/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.min.js"></script>
    <script>
        $(document).ready(function(){
           
				
				$('.bg-image, .post-wide .post-image, .post.single .post-image').each(function(){
					var src = $(this).children('img').attr('src');
					$(this).css('background-image','url('+src+')').children('img').hide();
				});
                
				
				
				var $bgVideo = $('.bg-video');
				if($bgVideo) {
					$bgVideo.YTPlayer();
				}
				if($(window).width() < 1200 && $bgVideo) {
					$bgVideo.prev('.bg-video-placeholder').show();
					$bgVideo.remove()
				}

			});
    </script>
</body>
</html>