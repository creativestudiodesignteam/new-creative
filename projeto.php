<!DOCTYPE html>
<html>
<head>
    <?php include("includes/head.php")?>
</head>
<body style="height:1000px;">

    <?php include("includes/load.php")?>
    <?php include("includes/menu.php")?>

	<!-- HERO SECTION --> 
	<section class="hero work col-md-12" style="background-image:url(img/singlework/summer-1.jpg)"> 
        <div class="hero-inner">
            <h1 class="title-project">Save The Nature</h1>
            <p class="description-project">Made for a restless generation <br>Making a youth brand young again </p>
        <div id="line-scroll" style="display: block;">
          <div class="line-scroll" id="lsl"> <span class="line active"></span> </div>
        </div>
        </div>
	</section>

	<div class="wrapper container">

	<!-- CONTENT SECTION -->	
	<section class="contact col-md-12 text-center">
        <!-- Big Image -->
        <div class="big-img col-md-12 bottom_60">
            <img src="img/singlework/summer-2.jpg" alt="">
        </div>
        <div class="col-md-12 work-video bottom_90">
            <iframe width="100%" src="https://www.youtube.com/embed/dC_l72dCgX8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>


        <!-- Controller -->
        <div class="controller col-md-10 col-md-offset-1 top_120">
            <a href="work-7.html" class="link pull-left text-left">
                <div class="where">Anterior </div>
                <span>The Space </span>
            </a>
            <div class="cont-line"></div>
            <a href="work-9.html" class="link pull-right text-right">
                <div class="where">Próximo </div>
                <span>Agência Back</span>
            </a>
        </div>

	</section>
	</div>

	<?php include("includes/footer.php")?>
	<?php include("includes/scripts.php")?>

</body>
</html>