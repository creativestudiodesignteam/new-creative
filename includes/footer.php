<footer class="top_120">
	   <div class="container">
            <div class="socials pull-left">
                <a href="index.php" class="footer-logo pull-left"><img src="img/logo.png" alt=""></a>
                <!-- <a href="mailto:contato@creativestudiodesigner.com.br">Email</a> -->
                <a href="https://www.behance.net/felipehenriquesilva" target="_blank">Behance</a>
                <a href="https://www.instagram.com/creativestudiodesigner/?hl=pt-br" target="_blank">Instagram</a>
                <a href="https://www.facebook.com/creativestudiodesigner/?modal=admin_todo_tour" target="_blank">Facebook</a>
                <a href="https://www.youtube.com/channel/UC4YEPXsMKZhHXnuAo3OEkyA" target="_blank">Youtube</a>
                <a href="https://api.whatsapp.com/send?phone=5511959806811&text=Olá%21%20Eu%20vim%20através%20do%20site%20e%20gostaria%20de%20solicitar%20um%20orçamento." target="_blank">Whatsapp</a>
            </div>
            <div class="copyright pull-right">
                Copyright 2019 Todos os direitos reservados. <span>Creative Dev & Design</span> 
            </div>
       </div>
	</footer>