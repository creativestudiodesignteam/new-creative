<!-- HEADER -->
<header>
    	<a href="index.php" class="logo pull-left">
			<img src="img/logo.png" alt="">
        </a>
		<div class="hamburger pull-right">
			<div id="nav-icon">
			  <span></span>
			  <span></span>
			  <span></span>
			</div>
		</div>
	</header>

	<div class="nav">
		<ul class="menu">
			<li><a href="index.html">Home</a></li>
			<li><a href="index.html">Portfólio</a></li>
			<li><a href="sobre.php">Sobre</a></li>
            <li><a href="blog.php">Blog</a></li>
			<li><a href="contato.php">Contato</a></li>
            <li class="social">
                <div class="social">
                    <a href="mailto:contato@creativestudiodesigner.com.br"><i class="fa fa-envelope-o" aria-hidden="true"></i>  </a>
                    <a href="https://www.instagram.com/creativestudiodesigner/?hl=pt-br" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i>  </a>
                    <a href="https://www.behance.net/felipehenriquesilva" target="_blank"><i class="fa fa-behance" aria-hidden="true"></i>  </a>
                    <a href="https://www.facebook.com/creativestudiodesigner/?modal=admin_todo_tour" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i>  </a>
                    <a href="https://www.youtube.com/channel/UC4YEPXsMKZhHXnuAo3OEkyA" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i>  </a>
                    <a href="https://api.whatsapp.com/send?phone=5511959806811&text=Olá%21%20Eu%20vim%20através%20do%20site%20e%20gostaria%20de%20solicitar%20um%20orçamento." target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i>  </a>
                </div>
            </li>
		</ul>
	</div>